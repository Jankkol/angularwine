'use strict';

/* Controllers */



wineApp.controller('addNewObligationsCtrl', function($scope, $http, $modalInstance, userFinanceFactory) {
    console.log("addNewObligationsCtrl");
    $scope.monthlyObligations = [{}];

    $scope.save = function() {
        $http.post('/saveNewObligations', {
            "userMonthlyObligations": $scope.monthlyObligations
        }).
            success(function(userFinanceCalculated) {
                userFinanceFactory.setFinanceData(userFinanceCalculated);
            }).error(function() {
                console.log("FAIL");
            });

        $modalInstance.close();
    };

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
    $scope.addFields = function() {

        $scope.monthlyObligations.push({});

    };

});