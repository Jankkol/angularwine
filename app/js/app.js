'use strict';

/* App Module */
var wineApp = angular.module('wineApp', [
    'ngRoute',
    'wineAppControllers'
]);


wineApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/centerPanelMainPage.html',
                controller: 'MainPanelController',
            }).
            when('/addWine', {
                templateUrl: 'partials/addWine.html',
            }).
            when('/addMeal', {
                templateUrl: 'partials/addMeal.html',
            }).
            when('/mealList', {
                templateUrl: 'partials/mealList.html',
            }).
            when('/wineList', {
                templateUrl: 'partials/wineList.html',
            }).
            when('/searchForWine', {
                templateUrl: 'partials/searchForWine.html',
            }).
            otherwise({
                redirectTo: '/error'
            });
    }
]);
