'use strict';

/* Controllers */

var wineApp = angular.module('wineAppControllers', []);


wineApp.controller('MainPanelController', ['$scope', '$http',
    function ($scope, $http) {
        console.log("MainPanelController");
    }
]);

wineApp.controller('AddWineController', ['$scope', '$http',
    function ($scope, $http) {
        console.log("AddWineController");
        $scope.addWine = function () {
            var wine = {wineName: $scope.wineName, wineFlavor: $scope.wineFlavor,
                wineSugar: $scope.wineSugar, wineColor: $scope.wineColor}
            $http.post('/saveNewWine', {wine: wine}).
                success(function (data) {
                    console.log("SUCCESS");
                }).error(function () {
                    console.log("FAIL");
                });
        }
    }
]);

wineApp.controller('AddMealController', ['$scope', '$http',
    function ($scope, $http) {
        console.log("AddWineController");
        $scope.addMeal = function () {
            var meal = {mealName: $scope.mealName, bestFlavor: $scope.bestFlavor,
                bestSugar: $scope.bestSugar, bestColor: $scope.bestColor};
            $http.post('/saveNewMeal', {meal: meal}).
                success(function (data) {
                    console.log("SUCCESS");
                }).error(function () {
                    console.log("FAIL");
                });
        }
    }
]);

wineApp.controller('MealListController', ['$scope', '$http',
    function ($scope, $http) {
        $http.get('/getMealList').
            success(function (data) {
                $scope.meals = data;
            }).error(function () {
                console.log("FAIL");
            });
    }
]);

wineApp.controller('WineListController', ['$scope', '$http',
    function ($scope, $http) {
        $http.get('/getWineList').
            success(function (data) {
                $scope.wines = data;
            }).error(function () {
                console.log("FAIL");
            });
    }
]);

wineApp.controller('SearchForWineController', ['$scope', '$http',
    function ($scope, $http) {
        console.log("AddWineController");
        $http.get('/getMealList').
            success(function (data) {
                $scope.options = data;
            }).error(function () {
                console.log("FAIL");
            });
        $scope.searchWine = function () {
            $http.post('/getWineForMeal', { meal: $scope.mealName2.mealName }).
                success(function (data) {
                    $scope.wines = data;
                }).error(function () {
                    console.log("FAIL");
                });
        }
    }
]);