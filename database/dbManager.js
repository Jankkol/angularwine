var Q = require('q');
var monk = require('monk');
var db = monk('localhost:27017/wineApp');


var dbConnectionHandler = function (status) {
    if (status) {
        console.dir("Brak polaczenia z baza");
    } else {
        console.log('Połączenie z bazą udane!');
    }
    return status;
}
exports.dbConnectionHandler = dbConnectionHandler;

var saveMeal = function (meal) {
    var deferred = Q.defer();
    var collection = db.get('mealCollectio');
    collection.insert({
        "mealName": meal.mealName,
        "bestFlavor": meal.bestFlavor,
        "bestSugar": meal.bestSugar,
        "bestColor": meal.bestColor
    }, function (err, doc) {
        if (err) {
            console.log(err + "!!!!!");
            deferred.reject();
        } else {
            console.log("OK");
            deferred.resolve();
        }
    });
    return deferred.promise;
}
exports.saveMeal = saveMeal;

var saveWine = function (wine) {
    var deferred = Q.defer();
    var collection = db.get('wineCollectio');
    collection.insert({
        "wineName": wine.wineName,
        "wineFlavor": wine.wineFlavor,
        "wineSugar": wine.wineSugar,
        "wineColor": wine.wineColor
    }, function (err, doc) {
        if (err) {
            console.log(err + "!!!!!");
            deferred.reject();
        } else {
            console.log("OK");
            deferred.resolve();
        }
    });
    return deferred.promise;
}
exports.saveWine = saveWine;

var checkIfWineNotExist = function (name) {
    var deferred = Q.defer();
    var collection = db.get('wineCollectio');
    collection.findOne({
        wineName: name
    }, function (err, wine) {
        if (wine === null) {
            deferred.resolve();
        } else {
            console.log(wine);
            deferred.reject();
        }
    });
    return deferred.promise;
}

exports.checkIfWineNotExist = checkIfWineNotExist;

var checkIfMealNotExist = function (name) {
    var deferred = Q.defer();
    var collection = db.get('mealCollectio');
    collection.findOne({
        mealName: name
    }, function (err, meal) {
        if (meal === null) {
            deferred.resolve();
        } else {
            console.log(meal);
            deferred.reject();
        }
    });
    return deferred.promise;
}

exports.checkIfMealNotExist = checkIfMealNotExist;

var getWineList = function () {
    var deferred = Q.defer();
    var collection = db.get('wineCollectio');
    collection.find( {}, function (err, wine) {
        if (wine === null) {
            deferred.reject();
        } else {
            deferred.resolve(wine);
        }
    });
    return deferred.promise;
}

exports.getWineList = getWineList;

var getMealList = function () {
    var deferred = Q.defer();
    var collection = db.get('mealCollectio');
    collection.find( {}, function (err, meal) {
        if (meal === null) {
            deferred.reject();
        } else {
            console.log(meal);
            deferred.resolve(meal);
        }
    });
    return deferred.promise;
}

exports.getMealList = getMealList;

var getMealByName = function (meal) {
    var deferred = Q.defer();
    var collection = db.get('mealCollectio');
    collection.find( { mealName : meal }, function (err, meal) {
        if (err) {
            deferred.reject();
        } else {
            deferred.resolve(meal);
        }
    });
    return deferred.promise;
}

exports.getMealByName = getMealByName;
