'use strict';

var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var mongo = require('mongodb');
var monk = require('monk');
var dbManager = require('./database/dbManager.js');
var Q = require('q');
var port = 3000;
var socketIo = require('socket.io');
var connect = require('connect');
var httpServer = require("http").createServer(app);
var io = socketIo.listen(httpServer);

mongo.connect('mongodb://localhost:27017/wineApp', dbManager.dbConnectionHandler);

app.use(express.static(__dirname + '/app'));
app.use(bodyParser.urlencoded({
    'extended': 'true'
}));
app.use(bodyParser.json());
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));


console.log("app listen on port " + port);

app.get('/', function (req, res) {
    res.sendfile("./app/mainPanel.html");
});

app.post('/saveNewWine', function (req, res) {
    dbManager.checkIfWineNotExist(req.body.wine.wineName).then(function () {
        dbManager.saveWine(req.body.wine).then(function (result) {
            res.json(result);
        });
    }).catch(function (error) {
        res.send(false);
    });
});

app.post('/saveNewMeal', function (req, res) {
    console.log(req.body.meal);
    dbManager.checkIfMealNotExist(req.body.meal.mealName).then(function () {
        dbManager.saveMeal(req.body.meal).then(function (result) {
            res.json(result);
        });
    }).catch(function (error) {
        res.send(false);
    });
});

app.get('/getWineList', function (req, res) {
    dbManager.getWineList().then(function (result) {
        res.json(result);
    });

});

app.get('/getMealList', function (req, res) {
    dbManager.getMealList().then(function (result) {
        res.json(result);
    });

});

app.post('/getWineForMeal', function (req, res) {
    console.log(req.body.meal)
    dbManager.getMealByName(req.body.meal).then(function (meal) {
        dbManager.getWineList().then(function (result) {
            var bestFlavor;
            var bestColor;
            var bestSugar;
            for (var key in meal) {
                bestFlavor = meal[key].bestFlavor;
                bestColor = meal[key].bestColor;
                bestSugar = meal[key].bestSugar;
            }
            var resultArray = [];
            for (var key in result) {
                var wine = {wineName: result[key].wineName, wineFlavor: result[key].wineFlavor,
                    wineColor: result[key].wineColor, wineSugar: result[key].wineSugar, winePercentage: 0 };
                if (result[key].wineFlavor === bestFlavor) {
                    wine.winePercentage = wine.winePercentage + 1;
                }
                if (result[key].wineColor === bestColor) {
                    wine.winePercentage = wine.winePercentage + 1;
                }
                if (result[key].wineSugar === bestSugar) {
                    wine.winePercentage = wine.winePercentage + 1;
                }
                switch (wine.winePercentage) {
                    case 1:
                        wine.winePercentage = 33;
                        break;
                    case 2:
                        wine.winePercentage = 66;
                        break;
                    case 3:
                        wine.winePercentage = 100;
                        break;
                }
                resultArray.push(wine);
            }
            res.json(resultArray);
        });
    });
});

httpServer.listen(port, function () {
    console.log('Express server listening on port ' + port);
});